package com.witho.thomas.fileplayer;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FileListAdapter extends ArrayAdapter<FileItem> {
    private Context mContext;
    private List<FileItem> mList = new ArrayList<>();

    public FileListAdapter(@NonNull Context context, @LayoutRes ArrayList<FileItem> list) {
        super(context, 0, list);
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) view = LayoutInflater.from(mContext).inflate(R.layout.listitem, parent, false);

        FileItem item = mList.get(position);
        TextView name = (TextView)view.findViewById(R.id.name);
        name.setText(item.name());
        TextView playCount = (TextView)view.findViewById(R.id.playCount);
        if (item.isDirectory()) {
            if (item.playCount() < 0) playCount.setText("<");
            else if (item.playCount() > 0) playCount.setText(">");
        }
        else playCount.setText(String.valueOf(item.playCount()));

        return view;
    }

}
