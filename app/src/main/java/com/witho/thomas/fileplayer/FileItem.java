package com.witho.thomas.fileplayer;

import java.util.Comparator;
import java.util.Date;

public class FileItem {
    private String mName;
    private boolean mIsDirectory;
    private int mPlayCount;
    private Date mCreationDate;

    FileItem(String name, boolean isDirectory, int playCount, Date creationDate) {
        mName = name;
        mIsDirectory = isDirectory;
        mPlayCount = playCount;
        mCreationDate = creationDate;
    }

    public static class FileItemComparator implements Comparator<FileItem>
    {
        enum CompareType { Name, Date, PlayCount };
        private CompareType mCompareType;
        private boolean mDescending;
        FileItemComparator(CompareType compareType, boolean descending) {
            mCompareType = compareType;
            mDescending = descending;
        }
        public int compare(FileItem left, FileItem right) {
            if (left.isDirectory() != right.isDirectory()) {
                return left.isDirectory() ? -1 : 1;
            }
            if (left.isDirectory() && left.playCount() != right.playCount()) {
                return left.playCount() - right.playCount();
            }
            int result = 0;
            switch (mCompareType) {

                case Name:
                    result = left.name().compareToIgnoreCase(right.name());
                    break;
                case Date:
                    result = Long.signum(left.creationDate().getTime() - right.creationDate().getTime());
                    if (result == 0) result = left.name().compareToIgnoreCase(right.name());
                    break;
                case PlayCount:
                    result = left.playCount() - right.playCount();
                    if (result == 0) result = left.name().compareToIgnoreCase(right.name());
                    break;
            }
            if (mDescending) result = -result;
            return result;
        }
    }

    public String name() {
        return mName;
    }
    public boolean isDirectory() { return mIsDirectory; }
    public int playCount() { return mPlayCount; }
    public Date creationDate() { return mCreationDate; }

}
