package com.witho.thomas.fileplayer;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import com.snappydb.DB;
import com.snappydb.SnappyDB;
import com.snappydb.SnappydbException;

public class FileBrowserActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static final String CURRENT_DIRECTORY = "currentDirectory";
    private int mActiveTab = 1;
    private ListView mListView;
    private ArrayList<FileItem> mFileList = new ArrayList<>();
    private FileListAdapter mFileListAdapter;
    private TextView mStatus;
    private ToggleButton mButtonPlayPause;
    private ToggleButton mButtonRepeat;
    private ToggleButton mButtonPlaylist;
    private ImageButton mButtonClear;
    private ToggleButton mButtonSortDirection;
    private Button mButtonSortType;
    private File mCurrentDir;
    private MediaPlayer mPlayer;
    private ArrayList<String> mPlaylist = new ArrayList<>();
    private ArrayAdapter<String> mPlaylistAdapter;
    private int mPlayIndex = 0;
    private boolean mPlayerReady = false;
    private DB db = null;
    FileItem.FileItemComparator.CompareType mCompareType = FileItem.FileItemComparator.CompareType.Name;
    boolean mCompareDescending = false;

    private Handler mTimer;
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                setStatus();
            } finally {
                mTimer.postDelayed(mStatusChecker, 1000);
            }
        }
    };

    class IgnoreCaseComparator implements Comparator<String> {
        public int compare(String strA, String strB) {
            return strA.compareToIgnoreCase(strB);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_file_browser);

        mPlaylistAdapter = new ArrayAdapter<String>(FileBrowserActivity.this, android.R.layout.simple_list_item_1, mPlaylist);
        mFileListAdapter = new FileListAdapter(FileBrowserActivity.this, mFileList);

        mPlayer = new MediaPlayer();
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mPlayerReady = false;
                mButtonPlayPause.setChecked(false);
                int playCount = 0;
                try {
                    if (db == null) tryOpenDb();
                    if (db != null) db.getInt(mPlaylist.get(mPlayIndex));
                } catch (SnappydbException e) {

                }
                playCount += 1;
                try {
                    if (db != null) db.putInt(mPlaylist.get(mPlayIndex), playCount);
                } catch (SnappydbException e) {
                }

                mPlayIndex++;
                if (mPlayIndex >= mPlaylist.size()) {
                    if (mButtonRepeat.isChecked()) mPlayIndex = 0;
                    else mButtonPlayPause.setChecked(false);
                }
                if (mPlayIndex < mPlaylist.size()) play(mPlayIndex);
            }
        });
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mPlayerReady = false;
                if (mPlayIndex < mPlaylist.size()) {
                    mPlaylist.remove(mPlayIndex);
                    if (mPlayIndex < mPlaylist.size()) play(mPlayIndex);
                }
                return false;
            }
        });
        mListView = findViewById(R.id.fileList);
        mStatus = findViewById(R.id.status);

        mButtonPlayPause = findViewById(R.id.buttonPlayPause);
        initButton(mButtonPlayPause, R.drawable.ic_pause, R.drawable.ic_play);
        mButtonPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) mPlayer.pause();
                else if (mPlayerReady) mPlayer.start();
                else play(mPlayIndex);
            }
        });

        mButtonRepeat = findViewById(R.id.buttonRepeat);
        initButton(mButtonRepeat, R.drawable.ic_repeat, R.drawable.ic_repeat);

        mButtonPlaylist = findViewById(R.id.buttonPlaylist);
        initButton(mButtonPlaylist, R.drawable.ic_playlist, R.drawable.ic_playlist);
        mButtonPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mButtonPlaylist.isChecked()) {
                    mListView.setAdapter(mPlaylistAdapter);
                }
                else {
                    mListView.setAdapter(mFileListAdapter);
                }
            }
        });

        mButtonClear = findViewById(R.id.buttonClear);
        mButtonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) mPlayer.stop();
                mButtonPlayPause.setChecked(false);
                mPlaylist.clear();
                mPlayIndex = 0;
                mPlayerReady = false;
                mPlaylistAdapter.notifyDataSetChanged();
            }
        });

        mButtonSortDirection = findViewById(R.id.buttonSortDirection);
        mCompareDescending = getPreferences(Context.MODE_PRIVATE).getBoolean("SortDirection", false);
        mButtonSortDirection.setChecked(mCompareDescending);
        mButtonSortDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCompareDescending = mButtonSortDirection.isChecked();
                SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putBoolean("SortDirection", mCompareDescending);
                edit.apply();
                sort();
            }
        });

        mButtonSortType = findViewById(R.id.buttonSortType);
        mCompareType = FileItem.FileItemComparator.CompareType.values()[getPreferences(Context.MODE_PRIVATE).getInt("SortType", FileItem.FileItemComparator.CompareType.Name.ordinal())];
        switch (mCompareType) {

            case Name: mButtonSortType.setText("ABC");
                break;
            case Date: mButtonSortType.setText("YMD");
                break;
            case PlayCount: mButtonSortType.setText("#");
                break;
        }
        mButtonSortType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mButtonSortType.getText().toString();
                if (text.equals("ABC")) {
                    mCompareType = FileItem.FileItemComparator.CompareType.Date;
                    mButtonSortType.setText("YMD");
                }
                else if (text.equals("YMD")) {
                    mCompareType = FileItem.FileItemComparator.CompareType.PlayCount;
                    mButtonSortType.setText("#");
                }
                else if (text.equals("#")) {
                    mCompareType = FileItem.FileItemComparator.CompareType.Name;
                    mButtonSortType.setText("ABC");
                }
                SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = pref.edit();
                edit.putInt("SortType", mCompareType.ordinal());
                edit.apply();
                sort();
            }
        });

        mButtonPlayPause = findViewById(R.id.buttonPlayPause);
        initButton(mButtonPlayPause, R.drawable.ic_pause, R.drawable.ic_play);
        mButtonPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) mPlayer.pause();
                else if (mPlayerReady) mPlayer.start();
                else play(mPlayIndex);
            }
        });

        setStatus();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mButtonPlaylist.isChecked()) {
                    mPlaylist.remove(position);
                    mPlaylistAdapter.notifyDataSetChanged();
                    if (position == mPlayIndex) {
                        if (mPlayIndex < mPlaylist.size()) play(mPlayIndex);
                        else mPlayer.reset();
                    }
                    else if (position < mPlayIndex) {
                        mPlayIndex--;
                    }
                }
                else {
                    FileItem sel = (FileItem)parent.getItemAtPosition(position);
                    File f = new File(mCurrentDir, sel.name());
                    if (f.isDirectory()) {
                        updateFileList(f);
                    } else {
                        try {
                            mPlaylist.add(f.getCanonicalPath());
                            setStatus();
                        } catch (IOException e) { }
                    }
                }

            }
        });
        mListView.setAdapter(mFileListAdapter);

        findViewById(R.id.buttonDir1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveTab = 1;
                updateTab();
            }
        });

        findViewById(R.id.buttonDir2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveTab = 2;
                updateTab();
            }
        });

        findViewById(R.id.buttonDir3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveTab = 3;
                updateTab();
            }
        });

        findViewById(R.id.buttonDir4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveTab = 4;
                updateTab();
            }
        });

        findViewById(R.id.buttonDir5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveTab = 5;
                updateTab();
            }
        });

        updateTab();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.need_permission_text);
                builder.setNeutralButton(R.string.need_permission_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions();
                        updateFileList(mCurrentDir);
                    }
                });
                builder.show();
            }
            else {
                requestPermissions();
                updateFileList(mCurrentDir);
            }
        }
        tryOpenDb();
        mTimer = new Handler();
        mStatusChecker.run();
    }

    @Override
    public void onDestroy() {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
        }
        super.onDestroy();
    }

    void tryOpenDb() {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), ".fileplayer");
            if (!file.mkdirs())  Toast.makeText(this, "Failed to create: " + file.getAbsolutePath(), 2).show();
            db = new SnappyDB.Builder(this)
                    .directory(file.getAbsolutePath())
                    .name("play_count")
                    .build();
        } catch (SnappydbException e) {
            Toast.makeText(this, e.getMessage(), 2).show();
        } catch (IllegalStateException e) {
            Toast.makeText(this, e.getMessage(), 2).show();
        }
    }
    void requestPermissions()
    {
        ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, MY_PERMISSIONS_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateFileList(mCurrentDir);
                }
            }
        }
    }

    void updateFileList(File nextDir)
    {
        File[] files = nextDir.listFiles();
        if (files == null || files.length == 0) return;
        mCurrentDir = nextDir;
        try {
            getPreferences(Context.MODE_PRIVATE).edit().putString(CURRENT_DIRECTORY + mActiveTab, mCurrentDir.getCanonicalPath()).apply();
            mFileList.clear();
            for (int i = 0; i < files.length; ++i) {
                File f = new File(mCurrentDir, files[i].getName());
                int count = 0;
                boolean isDirectory = false;
                boolean isPlayable = true;
                if (f.isDirectory()) {
                    count = 1;
                    isDirectory = true;
                } else {
                    int extPos = f.getName().lastIndexOf(".");
                    if (extPos > 0) {
                        String ext = f.getName().substring(extPos).toLowerCase();
                        if (ext.equals(".mp3")) {
                            count = 0;
                            try {
                                if (db != null) count = db.getInt(f.getCanonicalPath());
                            } catch (SnappydbException e) {
                            }
                        }
                        else isPlayable = false;
                    }
                    else isPlayable = false;
                }
                if (isPlayable) mFileList.add(new FileItem(files[i].getName(), isDirectory, count, new Date(f.lastModified())));
            }
            mFileList.add(0, new FileItem("..", true, -1, new Date()));
            sort();
        } catch (IOException e) { }
    }

    void sort() {
        mFileList.sort(new FileItem.FileItemComparator(mCompareType, mCompareDescending));
        mFileListAdapter.notifyDataSetChanged();
    }

    void setStatus()
    {
        if (!mPlaylist.isEmpty()) {
            if (mPlayerReady) {
                int progress = 100 * mPlayer.getCurrentPosition() / mPlayer.getDuration();
                File f = new File(mPlaylist.get(mPlayIndex));
                mStatus.setText((mPlayIndex + 1) + "/" + mPlaylist.size() + " at " + progress + "% of " + f.getName());
            } else {
                mStatus.setText(mPlaylist.size() + " files");
            }
        } else {
            mStatus.setText("Empty");
        }
    }

    void initButton(ToggleButton button, int resourceOn, int resourceOff)
    {
        ImageSpan imageSpan = new ImageSpan(this, resourceOn);
        SpannableString content = new SpannableString("X");
        content.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        button.setTextOn(content);
        imageSpan = new ImageSpan(this, resourceOff);
        content = new SpannableString("X");
        content.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        button.setTextOff(content);
        button.setText(content);
    }

    void play(int index) {
        try {
            mPlayer.reset();
            mPlayer.setDataSource(mPlaylist.get(index));
            mPlayer.prepare();
            mPlayer.start();
            mPlayerReady = true;
            mButtonPlayPause.setChecked(true);
        } catch (IOException ex) {
            mPlaylist.remove(index);
            if (index < mPlaylist.size()) play(index);
        }
    }

    void updateTab() {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        try {
            mCurrentDir = new File(pref.getString(CURRENT_DIRECTORY + mActiveTab, Environment.getExternalStorageDirectory().getCanonicalPath()));
            updateFileList(mCurrentDir);
        } catch (IOException e) { }

    }

}
